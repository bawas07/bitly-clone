const mongoose = require('mongoose')

const client = new mongoose.Schema({
    _url: {type: mongoose.Schema.ObjectId, ref: "Url"},
    email: String,
    password: String
})

module.exports = mongoose.model('Client', client)