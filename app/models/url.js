const mongoose = require('mongoose')

const counterSchema = mongoose.Schema({
    date: Date,
    ip: String
})

const urlSchema = mongoose.Schema({
    longUrl: String,
    creator_id : {type: mongoose.Schema.ObjectId, ref: 'User'},
    short:String,
    counter:[counterSchema]
})

module.exports = mongoose.model('Url', urlSchema)