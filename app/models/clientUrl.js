const mongoose = require('mongoose')

const counterSchema = new mongoose.Schema({
    date: Date,
    ip: String
})

const clientUrlSchema = new mongoose.Schema({
    longUrl: String,
    creator_id : {type: mongoose.Schema.ObjectId, ref: 'Client'},
    short:String,
    counter:[counterSchema]
})

module.exports = mongoose.model('ClientUrl', clientUrlSchema)