const shortUrlSchema = mongoose.Schema({
    _url: {type: Schema.ObjectId, ref: "Url"},
    shorten: String,
    counter: Number
})

module.exports = mongoose.model('ShortUrl', shortUrlSchema)