const mongoose = require('mongoose')

const counterSchema = mongoose.Schema({
    clickedDate: {type: Date, default:Date.now()},
    _url : { type: mongoose.Schema.ObjectId, ref: 'Url' }
})

module.exports = mongoose.model('Url', urlSchema)