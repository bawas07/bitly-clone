const Client = require('../models/client')
const ClientUrl = require('../models/clientUrl')

const unique = require('../../config/unique')

exports.getProfile = function(req, res){
    const data = {
        id:req.client.id,
        email:req.client.email
    }
    res.status(200).json({
        status: 'success',
        data:data
    })
}

exports.postProfile = function(req,res){
    console.log(req.body)
    let {url, clientId} = req.body
    let shortUrl = ''
    let check = ''
    do{
        shortUrl = unique()
        ClientUrl.find({short: shortUrl},function(err, res){
            if (err){
                console.log(err)
            }
            check = res.short
        })
    }while (shortUrl == check)

    ClientUrl.findOne({longUrl: url, creator_id: clientId}, function(err, doc){
        if (doc){
            console.log({'shortUrl': doc.Short})
            res.status(200).json({
                status:'failed',
                message:"you already registered this url"
            })
        }else{
            let newUrl = ClientUrl({
                longUrl: url,
                short: shortUrl,
                creator_id: clientId
            })

            newUrl.save(function(err){
                if(err){
                    console.log(err)
                }
                console.log('data saved')
                res.status(200).json({
                    status:'success',
                    data:{
                        Url:url,
                        Shorturl:shortUrl
                    }
                })
            })
        }
    })
}

exports.getUrlList = async function(req, res){
    let url
    const {id} = req.client
    let status = req.query.status
    const obj = {}
    const objday = {}
    let monthCounter = 0
    let dayCounter = 0
    const timedate = new Date(Date())
    let thisday = timedate.getDate()
    let thismonth = timedate.getMonth()
    let thisyear = timedate.getFullYear()
    await ClientUrl.find({creator_id: id},function(err, res){
        if (err){
            console.log(err)
        }
        url = res
    })
    
    for (data of url){
        for (click of data.counter){
            if (click.date.getMonth() == thismonth && click.date.getFullYear() == thisyear){
                monthCounter = monthCounter+1
            }
        }
        obj[data._id] = monthCounter
        monthCounter = 0
    }
    for (data of url){
        for (click of data.counter){
            if (click.date.getDate() == thisday && click.date.getMonth() == thismonth && click.date.getFullYear() == thisyear){
                dayCounter = dayCounter+1
            }
        }
        objday[data._id] = dayCounter
        dayCounter = 0
    }
    let x = [{longUrl:'long url', short:'short'}, {longUrl:'long url2', short:'short2'}]
    res.status(200).json({
        status: "success",
        data: {
            user : req.user, 
            url : url,
            obj:obj,
            objday:objday,
            status:status
        }        
    })
}

exports.postUrlList = async function(req, res){
    let {id, short} = req.body
    console.log(id, short)
    await ClientUrl.find({short: short},function(err, doc){
        if (err){
            console.log(err)
        }
        console.log('doc')
        if (doc[0]!==undefined){
            let status = 'taken'
            console.log('masuk if')
            console.log('this value is taken, please type another short link')
            res.redirect('/url-list?status=${status}')
        }
        else{
            console.log('masuk else')
            ClientUrl.findOneAndUpdate({_id:id}, {$set:{short:short}},function(err,doc){
                if (err){
                    console.log(err)
                }
                res.redirect('/url-list')
            })
        }
    })
    await Url.find({_id: req._id},function(err, res){
        if (err){
            console.log(err)
        }
        url = res
    })
    
    let x = [{longUrl:'long url', short:'short'}, {longUrl:'long url2', short:'short2'}]
    res.status(200).json({
        status: 'success',
        data: {
            user: req.user,
            url:url
        }
    })
    // res.render('url-list.ejs', {
    //     user : req.user, 
    //     url : url
    // })
}

exports.getUrlListId = function(req, res){
    const id = req.params.id
    ClientUrl.findOne({_id: id},function(err, doc){
        if (err){
            console.log(err)
        }
        // let x = doc
        // console.log(doc[0]._id)
        res.status(200).json({
            status: 'success',
            data:{
                doc: doc
            }
        })
        // res.render('statistic.ejs', {doc: doc[0]})
    })
}