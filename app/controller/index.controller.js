const ClientUrl = require('../models/clientUrl')

exports.getIndex = function(req,res){
    let {url,short} = req.query
    res.render('index.ejs', {url:url, short:short})
}

exports.postIndex = function(req,res){
    console.log(req.body)
    let oriUrl = req.body.url
    let shortUrl = ''
    let check = ''
    do{
        shortUrl = unique()
        ClientUrl.find({short: shortUrl},function(err, res){
            if (err){
                console.log(err)
            }
            check = res.short
        })
    }while (shortUrl == check)

    let newUrl = ClientUrl({
        longUrl: oriUrl,
        short: shortUrl,
        counter:[]
    })

    newUrl.save(function(err){
        if(err){
            console.log(err)
        }
        console.log('data saved')
        
    })
    res.status(200).json({
        url:oriUrl,
        short:shortUrl
    })
    // res.redirect(`/?url=${oriUrl}&short=${shortUrl}`)
}

exports.redirectUrl = function(req, res){
    const hash = req.params.hash
    ClientUrl.findOneAndUpdate({short:hash}, {$push:{counter:{date:Date.now(), ip:req.ip}}},function(err,doc){
        if (err){
            console.log(err)
        }
        console.log(req.socket.remoteAddress)
        if(!doc.longUrl){
            res.redirect('/')
        }
        const url = doc.longUrl.split('://')
        if (url.includes('http') || url.includes('https')){
            const realUrl = url[1].split('www.')
        }else{
            const realUrl = url[0].split('www.')
        }
        res.redirect('http://'+realUrl)
    })
}