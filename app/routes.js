const Url = require('./models/url')
//const Short = require('./models/short')
//const alias = require('../config/alias')
const shorthast = require('shorthash')
const unique = require('../config/unique')
const { signin, signup, checkAuth } = require('../config/jewete')
const { getProfile, postProfile, getUrlList, postUrlList, getUrlListId } = require('./controller/client.controller')
const { getIndex, postIndex, redirectUrl } = require('./controller/index.controller')


module.exports = function(app, passport){
    app.enable('trust proxy');
    // HOME PAGE
    app.get('/', getIndex)

    
    app.post('/', postIndex)

    // LOGIN
    // app.get('/login', function(req, res) {
       
    //     res.render('login.ejs', { message: req.flash('loginMessage') })
    // })

    // app.post('/login', passport.authenticate('local-login', {
    //     successRedirect: '/profile',
    //     failureRedirect: '/login',
    //     failureFlash: true
    // }))
    app.post('/signin', signin)

    // SIGNUP
    app.get('/signup', function(req, res){
        res.render('signup.ejs', { message: req.flash('signupMessage')})
    })

    // app.post('/signup', passport.authenticate('local-signup',{
    //     successRedirect: '/profile',
    //     failureRedirect: '/signup',
    //     failureFlash: true
    // }))
    app.post('/signup', signup)

    // PROFILE SECTION
    // app.get('/profile', isLoggedIn, function(req, res){
    //     res.render('profile.ejs', {
    //         user : req.user 
    //     })
    // })
    app.get('/profile', checkAuth, getProfile)

    app.post('/profile', checkAuth, postProfile)

    // app.post('/profile', isLoggedIn, function(req,res){
    //     console.log(req.body)
    //     let {url, UserId} = req.body
    //     let shortUrl = ''
    //     let check = ''
    //     do{
    //         shortUrl = unique()
    //         Url.find({short: shortUrl},function(err, res){
    //             if (err){
    //                 console.log(err)
    //             }
    //             check = res.short
    //         })
    //     }while (shortUrl == check)

    //     Url.findOne({longUrl: url, creator_id_creator: UserId}, function(err, doc){
    //         if (doc){
    //             console.log({'shortUrl': doc.Short})
    //         }else{
    //             let newUrl = Url({
    //                 longUrl: url,
    //                 short: shortUrl,
    //                 creator_id: UserId
    //             })

    //             newUrl.save(function(err){
    //                 if(err){
    //                     console.log(err)
    //                 }
    //                 console.log('data saved')
                    
    //             })
    //         }
    //     })
    //     res.redirect('/url-list')
    // })


    app.get('/url-list', checkAuth, getUrlList)
    // app.get('/url-list', checkAuth, async function(req, res){
    //     let url
    //     let status = req.query.status
    //     const obj = {}
    //     const objday = {}
    //     let monthCounter = 0
    //     let dayCounter = 0
    //     const timedate = new Date(Date())
    //     let thisday = timedate.getDate()
    //     let thismonth = timedate.getMonth()
    //     let thisyear = timedate.getFullYear()
    //     await Url.find({creator_id: req.user._id},function(err, res){
    //         if (err){
    //             console.log(err)
    //         }
    //         url = res
    //     })
    //     for (data of url){
    //         for (click of data.counter){
    //             if (click.date.getMonth() == thismonth && click.date.getFullYear() == thisyear){
    //                 monthCounter = monthCounter+1
    //             }
    //         }
    //         obj[data._id] = monthCounter
    //         monthCounter = 0
    //     }
    //     for (data of url){
    //         for (click of data.counter){
    //             if (click.date.getDate() == thisday && click.date.getMonth() == thismonth && click.date.getFullYear() == thisyear){
    //                 dayCounter = dayCounter+1
    //             }
    //         }
    //         objday[data._id] = dayCounter
    //         dayCounter = 0
    //     }
    //     console.log(obj)
    //     let x = [{longUrl:'long url', short:'short'}, {longUrl:'long url2', short:'short2'}]
    //     res.render('url-list.ejs', {
    //         user : req.user, 
    //         url : url,
    //         obj:obj,
    //         objday:objday,
    //         status:status
    //     })
    // })


    app.post('/url-list', checkAuth, postUrlList)
    // app.post('/url-list', isLoggedIn, async function(req, res){
    //     let {id, short} = req.body
    //     await Url.find({short: short},function(err, doc){
    //         if (err){
    //             console.log(err)
    //         }
    //         console.log('doc')
    //         if (doc[0]!==undefined){
    //             let status = 'taken'
    //             console.log('masuk if')
    //             console.log('this value is taken, please type another short link')
    //             res.redirect('/url-list?status=${status}')
    //         }
    //         else{
    //             console.log('masuk else')
    //             Url.findOneAndUpdate({_id:id}, {$set:{short:short}},function(err,doc){
    //                 if (err){
    //                     console.log(err)
    //                 }
    //                 res.redirect('/url-list')
    //             })
    //         }
    //     })

    //     await Url.find({_id: req._id},function(err, res){
    //         if (err){
    //             console.log(err)
    //         }
    //         url = res
    //     })
        
    //     let x = [{longUrl:'long url', short:'short'}, {longUrl:'long url2', short:'short2'}]
    //     res.render('url-list.ejs', {
    //         user : req.user, 
    //         url : url
    //     })
    // })

    // LOGOUT
    // app.get('/logout', function(req, res){
    //     req.logout()
    //     res.redirect('/')
    // })

    app.get('/url-list/:id', checkAuth, getUrlListId)
    // app.get('/url-list/:id', isLoggedIn, function(req, res){
    //     const id = req.params.id
    //     Url.find({_id: id},function(err, doc){
    //         if (err){
    //             console.log(err)
    //         }
    //         let x = doc
    //         console.log(doc[0]._id)
    //         res.render('statistic.ejs', {doc: doc[0]})
    //     })
    // })

    app.get('/:hash', redirectUrl)
    // app.get('/:hash', function(req, res){
    //     const hash = req.params.hash
    //     Url.findOneAndUpdate({short:hash}, {$push:{counter:{date:Date.now(), ip:req.ip}}},function(err,doc){
    //         if (err){
    //             console.log(err)
    //         }
    //         console.log(req.socket.remoteAddress)
    //         if(!doc.longUrl){
    //             res.redirect('/')
    //         }
    //         console.log(req.socket.localAddress)
    //         console.log('ip='+req.ips)
    //         res.redirect('http://'+doc.longUrl)
    //     })
    // })


}

// function isLoggedIn(req, res, next){
//     if (req.isAuthenticated())
//         return next()

//     res.redirect('/')
// }