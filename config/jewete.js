const Client = require('../app/models/client')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const saltRound = 7
const {key} = require('./keyword')

exports.signup = async function(req, res){
    const {email, password} = req.body
    try{
        const hash = await bcrypt.hash(password, saltRound)
        const client = await Client.find({email: email})
        if(client.length>0){
            console.log(client)
            throw new Error('Email has been used')            
        }
        console.log(client)
        const newClient = new Client()
        newClient.email = email
        newClient.password = hash
        await newClient.save()
        res.status(200).json({
            status: 'success',
            message: 'Data has been saved'
        })
    }catch(err){
        console.log(err)
        res.status(401).json({
            status: "failed",
            error:err.message
        })
    }
    
}

exports.signin = async function(req, res){
    const { email, password }  = req.body
    try{
        const user = await Client.findOne({email:email})
        if(!user){
            throw new Error('no user found')
        }
        const status = await bcrypt.compare(password, user.password)
        if(!status){
            console.log(status)
            throw new Error('Incorrect password')
        }
        const token = jwt.sign({email:user.email, id:user._id}, key)
        res.status(200).json({
            status: 'success',
            token: token
        })
    }catch(err){
        console.log(err)
        res.status(401).json({
            status: "failed",
            error:err.message
        })
    }
    
}

exports.checkAuth = function(req, res, next){
    const token = req.headers.authorization
    if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' })
    jwt.verify(token, key, function(err, decoded) {
        if (err) return res.status(500).send({ status: "failed", message: 'Failed to authenticate token.' })
        req.client = {
            id: decoded.id,
            email: decoded.email
        }
        next()
    })
}